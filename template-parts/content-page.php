<?php
/**
 * Template part for displaying page content in page.php.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Grüner
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>


	<?php if(is_page( 'design-moebel-outlet' )){ ?>
	<!-- DESIGN MÖBEL OUTLET -->

		<div class="entry-content">

			<?php
				$x = new SimpleXmlElement(file_get_contents("http://design-moebeloutlet.de/?cat=". get_field('feld_1') ."&feed=rss2"));
				foreach($x->channel->item as $entry) {
					$entry_link = $entry->link;
					$entry_title = $entry->title;
					$entry_cat = preg_replace('/<!\[CDATA\[(.+)\]\]>/Uism', '$1', $entry->category);
					break;
				}
			?>
			<a href="<?php echo $entry_link; ?>" class="dmo-box-link" target="_blank">
				<div class="dmo-box dmo<?php the_field('feld_1'); ?>">
					<div class="overlay"></div>
					<div class="content">
						<h1><?php echo $entry_cat; ?></h1>
						<p><?php echo $entry_title; ?></p>
					</div>
				</div>
			</a>

			<?php
				$x = new SimpleXmlElement(file_get_contents("http://design-moebeloutlet.de/?cat=". get_field('feld_2') ."&feed=rss2"));
				foreach($x->channel->item as $entry) {
					$entry_link = $entry->link;
					$entry_title = $entry->title;
					$entry_cat = preg_replace('/<!\[CDATA\[(.+)\]\]>/Uism', '$1', $entry->category);
					break;
				}
			?>
			<a href="<?php echo $entry_link; ?>" class="dmo-box-link" target="_blank">
				<div class="dmo-box dmo<?php the_field('feld_2'); ?>">
					<div class="overlay"></div>
					<div class="content">
						<h1><?php echo $entry_cat; ?></h1>
						<p><?php echo $entry_title; ?></p>
					</div>
				</div>
			</a>

			<div class="dmo-box dmo-logo"></div>

			<?php
				$x = new SimpleXmlElement(file_get_contents("http://design-moebeloutlet.de/?cat=". get_field('feld_3') ."&feed=rss2"));
				foreach($x->channel->item as $entry) {
				$entry_link = $entry->link;
				$entry_title = $entry->title;
				$entry_cat = preg_replace('/<!\[CDATA\[(.+)\]\]>/Uism', '$1', $entry->category);
				break;
				}
			?>
			<a href="<?php echo $entry_link; ?>" class="dmo-box-link" target="_blank">
				<div class="dmo-box dmo<?php the_field('feld_3'); ?>">
					<div class="overlay"></div>
					<div class="content">
						<h1><?php echo $entry_cat; ?></h1>
						<p><?php echo $entry_title; ?></p>
					</div>
				</div>
			</a>

		</div>


	<?php } elseif(is_page( 'newsletter' )){ ?>
	<!-- NEWSLETTER -->

		<?php if (count(get_field('bg-slider')) > 1) { $slider_class = "has_slider";} ?>
		<div id="slides" class="bg-slider referenzen <?php echo $slider_class; ?>">
			<ul class="slides-container kontakt">
				<?php
				// name of gallery-field
				$images = get_field('bg-slider');
				// check if image is not empty
				if( $images ):
					// loop through the images
					foreach( $images as $image ): ?>
						<li>
							<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>">
							<div class="tooltip-content-fullscreen">
								<div class="desc">
									<div class="desc_relative">
										<div class="entry-content">
											<div class="newsletter_headline">
												<?php the_field('newsletter_headline'); ?>
											</div>
											<div class="newsletter-form">
												<?php the_field('newsletter_shortcode'); ?>
											</div>
										</div>
									</div>
								</div>
							</div>
						</li>
					<?php
					endforeach;
				endif;
				?>
			</ul>
			<nav class="slides-navigation">
				<a href="#" class="next">Next</a>
				<a href="#" class="prev">Previous</a>
			</nav>
		</div>



		
		

	<?php } elseif(is_page( 'ref' )){ ?>
	<!-- REFERENZEN ÜBERSICHT -->

		<div class="entry-content">
			<div class="ref_disc">
				Lassen Sie sich von unseren realisierten Projekten inspirieren.
				Unsere Filter vereinfachen Ihnen die Suche.
			</div>
			<?php the_field('referenzen_shortcode'); ?>
			<div class="ref_mobile_overlay">
				<div class="ref_mobile_applyFilter">Filter anwenden</div>
			</div>
			<div class="ref_mobile_filter"><img src="<?php bloginfo( 'template_url' ); ?>/img/gruener_icon_filter.svg" /></div>
		</div>


	<?php } elseif(is_page( 'termin' ) || is_page( 'terminvereinbarung' ) || is_page( 'terminvereinbarung-2' ) || is_page( 'terminvereinbarung-3' ) || is_page( 'terminvereinbarung-4' ) || is_page( 'terminvereinbarung-5' ) || is_page( 'gewinnspiel' )){ ?>
	<!-- TERMINVEREINBARUNG -->

		<?php if (count(get_field('bg-slider')) > 1) { $slider_class = "has_slider";} ?>
		<div id="slides" class="bg-slider referenzen <?php echo $slider_class; ?>">
			<ul class="slides-container kontakt">
				<?php
				// name of gallery-field
				$images = get_field('bg-slider');
				// check if image is not empty
				if( $images ):
					// loop through the images
					foreach( $images as $image ): ?>
						<li>
							<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>">
							<div class="tooltip-content-fullscreen">
								<div class="desc">
									<div class="desc_relative">
										<div class="entry-content">
											<?php the_field('form_shortcode'); ?>
										</div>
									</div>
								</div>
							</div>
						</li>
					<?php
					endforeach;
				endif;
				?>
			</ul>
			<nav class="slides-navigation">
				<a href="#" class="next">Next</a>
				<a href="#" class="prev">Previous</a>
			</nav>
		</div>




	<?php } elseif(is_page( 'impressum' ) || is_page( 'datenschutz' )) { ?>
	<!-- IMPRESSUM & DATENSCHUTZ -->

		<?php
			if (count(get_field('bg-slider')) > 1) { $slider_class = "has_slider";}
			if (is_page( 'impressum' )) {$imdat_class = "impressum";}
			if (is_page( 'datenschutz' )) {$imdat_class = "datenschutz";}
		?>
		<div id="slides" class="bg-slider referenzen <?php echo $slider_class; ?>">
			<ul class="slides-container <?php echo $imdat_class; ?>">
				<?php
				// name of gallery-field
				$images = get_field('bg-slider');
				// check if image is not empty
				if( $images ):
					// loop through the images
					foreach( $images as $image ): ?>
					<li>
						<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>">
						<div class="tooltip-content-fullscreen">
							<div class="desc">
								<div class="desc_relative">
									<span class="title"><?php the_field('headline'); ?></span></ br>
									<?php the_field('description'); ?>
								</div>
							</div>
						</div>
					</li>
					<?php
					endforeach;
				endif;
				?>
			</ul>
			<nav class="slides-navigation">
				<a href="#" class="next">Next</a>
				<a href="#" class="prev">Previous</a>
			</nav>
		</div>


	<?php } elseif(is_page( 'backoffice' ) || is_page( 'planung-und-beratung' ) || is_page( 'werkstatt' ) ) { ?>
	<!-- BACKOFFICE, PLANUNG UND BERATUNG, WERKSTATT -->


		<?php if (count(get_field('bg-slider')) > 1) { $slider_class = "has_slider";} ?>
		<div id="slides" class="bg-slider events special-event team <?php echo $slider_class; ?>">
			<ul class="slides-container">
			<?php
				$image_count = 0;
				$images = get_field('bg-slider');
				if( $images ): foreach( $images as $image ): $image_count++;?>
				<li class="<?php echo "slide_nr_" . $image_count; ?>">
					<img src="<?php echo $image['url']; ?>" class="preserve" alt="<?php echo $image['alt']; ?>">
					<div class="tooltip-wrapper">
						<div class="tooltip active">
							<div class="tooltip-content team visible">
								<div class="title"><?php echo $image['title']; ?></div></ br>
								<div class="desc"><?php echo $image['description']; ?></div></ br></ br>
								<div class="desc"><?php echo $image['caption']; ?></div>
							</div>
							<div class="tooltip-clickable"></div>
						</div>
					</div>
					<div class="team-content team">
						<div class="title"><?php echo $image['title']; ?></div>
						<div class="desc"><?php echo $image['description']; ?></div>
						<div class="desc"><?php echo $image['caption']; ?></div>
					</div>
				</li>
				<?php endforeach; endif; ?>
			</ul>
			<nav class="slides-navigation">
				<a href="#" class="next">Next</a>
				<a href="#" class="prev">Previous</a>
			</nav>
		</div>
		
	<?php } elseif(is_page('kontakt')) { ?>
	<!-- KONTAKT -->

		<?php $kontaktbild = get_field('kontaktbild'); ?>
		<?php if (count(get_field('bg-slider')) > 1) { $slider_class = "has_slider";} ?>
		<div id="slides" class="bg-slider referenzen <?php echo $slider_class; ?>">
			<ul class="slides-container kontakt">
				<?php
				// name of gallery-field
				$images = get_field('bg-slider');
				// check if image is not empty
				if( $images ):
					// loop through the images
					foreach( $images as $image ): ?>
						<li>
							<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>">
							<div class="tooltip-content-fullscreen">
								<div class="desc">
									<div class="desc_relative">
										<div class="title"><?php the_field('headline'); ?></div></ br>
										<div class="kontaktdaten"><?php the_field('description'); ?></div></ br>
										<div class="ansprechpartner">
											<img src="<?php echo $kontaktbild['url']; ?>" alt="<?php echo $kontaktbild['alt']; ?>" class="kontaktbild"></ br>
											<div class="ansprechpartner_content"><?php the_field('description2'); ?></div></ br>
										</div>
										<div class="oeffnungszeiten"><?php the_field('description3'); ?></div></ br>
									</div>
								</div>
							</div>
						</li>
					<?php
					endforeach;
				endif;
				?>
			</ul>
			<nav class="slides-navigation">
				<a href="#" class="next">Next</a>
				<a href="#" class="prev">Previous</a>
			</nav>
		</div>


	<?php } elseif(is_page( 'startseite' )) { ?>
	<!-- STARTSEITE -->
		
		<?php if (count(get_field('bg-slider')) > 1) { $slider_class = "has_slider";} ?>
			<?php if(get_field('latest_event')) { ?>
				<div id="slides" class="bg-slider referenzen <?php echo $slider_class; ?>">
					<ul class="slides-container">
						<?php
							$event_filter = get_field('event_filter');
							$post = $event_filter;
							setup_postdata( $post );
							$images = get_field('bg-slider');
							$image_id = $images[0];
							if( $image_id ):
								?>
								<li>
									<a href="<?php the_permalink(); ?>/#2"><img src="<?php echo $image_id['url']; ?>" alt="<?php echo $image_id['alt']; ?>"></a>
									<div class="tooltip-wrapper">
										<div class="tooltip active">
											<div class="tooltip-clickable"></div>
										</div>
									</div>
									<div class="tooltip-content-fullscreen">
										<div class="desc">
											<div class="desc_relative">
												<span class="title"><?php the_field('headline'); ?></span></ br>
												<?php the_field('description'); ?>
											</div>
										</div>
									</div>
								</li>
								<?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly
							endif;
						?>
					</ul>
					<div class="to_ref"><a href="/ref">zu unseren Referenzen</a></div>
					<nav class="slides-navigation">
						<a href="#" class="next">Next</a>
						<a href="#" class="prev">Previous</a>
					</nav>
				</div>
			<?php } else { ?>
				<div id="slides" class="bg-slider referenzen <?php echo $slider_class; ?>">
					<ul class="slides-container">
						<?php
							
							/* Normal Slider w/o Shuffle */
							/*					
							if( have_rows('referenzen_filter') ):

								while ( have_rows('referenzen_filter') ) : the_row();
									$post_object = get_sub_field('referenz_nr');
									if( $post_object ) :
										$post = $post_object;
										setup_postdata($post);	
										$bg_images = get_field('bg-slider');
										$bg_image = $bg_images[0];
										?>
										<li>
											<img src="<?php echo $bg_image['url']; ?>">
										</li>
										<?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
									<?php endif; ?> 
								<?php
								endwhile;
							endif;
							*/			
							

							$rows = get_field('referenzen_filter');
							if($rows)
							{
								shuffle( $rows );

								$row = $rows[0];
								$hersteller_nr = $row['referenz_nr'];
								$post = $hersteller_nr;
								setup_postdata( $post );

									$bg_images = get_field('bg-slider');
									$bg_image = $bg_images[0];

									?>
										<li>
											<!--<a href="<?php the_permalink(); ?>"><img src="<?php echo $bg_image['url']; ?>" ></a>-->
											<img src="<?php echo $bg_image['url']; ?>" >
										</li>

									<?php

								wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly
							}
						?>
					</ul>
					<div class="to_ref"><a href="/ref">zu unseren Referenzen</a></div>
					<nav class="slides-navigation">
						<a href="#" class="next">Next</a>
						<a href="#" class="prev">Previous</a>
					</nav>
				</div>
			<?php } ?>


			

	<?php } else {?>
	<!-- DEFAULT PAGE LAYOUT -->

		<?php if (count(get_field('bg-slider')) > 1) { $slider_class = "has_slider";} ?>
		<div id="slides" class="bg-slider referenzen <?php echo $slider_class; ?>">
			<ul class="slides-container">
				<?php $images = get_field('bg-slider');
				if( $images ): foreach( $images as $image ): ?>
					<li>
						<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>">
					</li>
				<?php endforeach; endif; ?>
			</ul>
			<nav class="slides-navigation">
				<a href="#" class="next">Next</a>
				<a href="#" class="prev">Previous</a>
			</nav>
		</div>
		<div class="tooltip-wrapper">
			<div class="tooltip active">
				<?php if (!get_field('fullscreen_content') ) : ?>
					<div class="tooltip-content visible">
						<div class="title"><?php if(get_field('headline')){the_field('headline');}else{the_title();}; ?></div></ br>
						<div class="desc"><?php the_field('description'); ?></div>
					</div>
				<?php endif; ?>
				<div class="tooltip-clickable"></div>
			</div>
		</div>
		<div class="tooltip-content-fullscreen-mobile">
			<div class="desc">
				<span class="title"><?php the_field('headline'); ?></span></ br>
				<?php the_field('description'); ?></div>
			</div>
		</div>

	<?php } ?>

</article>

<?php if (get_field('fullscreen_content') && !is_page('backoffice') && !is_page('werkstatt') && !is_page('planung-und-beratung')  && !is_page('bulthaup-bei-gruener') )   : ?>
	<div class="tooltip-content-fullscreen">
		<div class="desc"><span class="title"><?php the_field('headline'); ?></span></ br>
			<?php the_field('description'); ?></div>
		</div>
	</div>
<?php endif; ?>
