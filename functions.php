<?php
/**
 * Grüner functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Grüner
 */


 // This theme uses post thumbnails
add_theme_support( 'post-thumbnails' );

if ( ! function_exists( 'gruener_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function gruener_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on Grüner, use a find and replace
	 * to change 'gruener' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'gruener', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary Menu', 'gruener' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 * See https://developer.wordpress.org/themes/functionality/post-formats/
	 */
	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'gruener_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif; // gruener_setup
add_action( 'after_setup_theme', 'gruener_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function gruener_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'gruener_content_width', 640 );
}
add_action( 'after_setup_theme', 'gruener_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function gruener_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'gruener' ),
		'id'            => 'sidebar-1',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'gruener_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function gruener_scripts() {
	wp_enqueue_style( 'gruener-style', get_stylesheet_uri() );

	wp_enqueue_script( 'gruener-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20120206', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	wp_enqueue_script( 'animate-enhanced', get_template_directory_uri() . '/js/jquery.animate-enhanced.min.js', array ( 'jquery' ), 1.1, true);

	wp_enqueue_script( 'superslides', get_template_directory_uri() . '/js/jquery.superslides.js', array ( 'jquery' ), 1.1, true);

	wp_enqueue_script( 'touchSwipe', get_template_directory_uri() . '/js/jquery.touchSwipe.js', array ( 'jquery' ), 1.1, true);

	wp_enqueue_script( 'cookie', get_template_directory_uri() . '/js/jquery.cookie.min.js', array ( 'jquery' ), 1.1, true);

	wp_enqueue_script( 'navgoco', get_template_directory_uri() . '/js/jquery.navgoco.min.js', array ( 'jquery' ), 1.1, true);

	wp_enqueue_script( 'js-cookie', get_template_directory_uri() . '/js/js.cookie.min.js', array ( 'jquery' ), 1.1, true);

	wp_enqueue_script( 'gruener-scripts', get_template_directory_uri() . '/js/gruener-scripts.js', array ( 'jquery' ), 1.1, true);

}
add_action( 'wp_enqueue_scripts', 'gruener_scripts' );

/**
 * Disable Admin Bar
 */
add_filter('show_admin_bar', '__return_false');

/**
 * load latest version of JQuery from Google
 */
if( !is_admin() ){
	wp_deregister_script('jquery');
	wp_register_script('jquery', get_template_directory_uri() . '/js/jquery-3.6.3.min.js', false, '');

	wp_enqueue_script('jquery');
}
if( !is_admin() ){
	wp_deregister_script('jquery-ui');
	wp_register_script('jquery-ui', get_template_directory_uri() . '/js/jquery-ui.min.js', false, '');
	wp_enqueue_script('jquery-ui');
}
 
/**
 * add new image-size
 */
add_image_size( 'smartphone', 960, 640 );
add_image_size( 'tablet', 1024, 768 );
add_image_size( 'desktop', 1440, 900 );
add_image_size( 'retina', 2880, 1880 );

/**
 * add slug as Body-class
 */
function add_slug_body_class( $classes ) {
	global $post;
	if ( isset( $post ) ) {
		$classes[] = $post->post_name;
	}
	return $classes;
}
add_filter( 'body_class', 'add_slug_body_class' );

/**
 * get RSS-Feed
 */
function getRssfeed($rssfeed, $cssclass="", $encode="auto", $anzahl=10, $mode=0)
{
	// $encode e[".*"; "no"; "auto"]

	// $mode e[0; 1; 2; 3]:
	// 0 = nur Titel und Link der Items weden ausgegeben
	// 1 = Titel und Link zum Channel werden ausgegeben
	// 2 = Titel, Link und Beschreibung der Items werden ausgegeben
	// 3 = 1 & 2

	// Zugriff auf den RSS Feed
	$data = @file($rssfeed);
	$data = implode ("", $data);
	if(strpos($data,"</item>") > 0)
	{
		preg_match_all("/<item.*>(.+)<\/item>/Uism", $data, $items);
		$atom = 0;
	}
	elseif(strpos($data,"</entry>") > 0)
	{
		preg_match_all("/<entry.*>(.+)<\/entry>/Uism", $data, $items);
		$atom = 1;
	}

	// Encodierung
	if($encode == "auto")
	{
		preg_match("/<?xml.*encoding=\"(.+)\".*?>/Uism", $data, $encodingarray);
		$encoding = $encodingarray[1];
	}
	else
	{$encoding = $encode;}

	echo "<div class=\"rssfeed_".$cssclass."\">\n";

	// Titel und Link zum Channel
	if($mode == 1 || $mode == 3)
	{
		if(strpos($data,"</item>") > 0)
		{
			$data = preg_replace("/<item.*>(.+)<\/item>/Uism", '', $data);
		}
		else
		{
			$data = preg_replace("/<entry.*>(.+)<\/entry>/Uism", '', $data);
		}
		preg_match("/<title.*>(.+)<\/title>/Uism", $data, $channeltitle);
		if($atom == 0)
		{
			preg_match("/<link>(.+)<\/link>/Uism", $data, $channellink);
		}
		elseif($atom == 1)
		{
			preg_match("/<link.*alternate.*text\/html.*href=[\"\'](.+)[\"\'].*\/>/Uism", $data, $channellink);
		}

		$channeltitle = preg_replace('/<!\[CDATA\[(.+)\]\]>/Uism', '$1', $channeltitle);
		$channellink = preg_replace('/<!\[CDATA\[(.+)\]\]>/Uism', '$1', $channellink);

		$rss_cat = $channeltitle[1];
		$replace = "design möbel outlet &#187; ";
		$rss_cat = str_replace($replace, "", $rss_cat);
		$rss_cat = html_entity_decode($rss_cat);

		echo "<h1>";
		if($encode != "no")
		{echo htmlentities($rss_cat,ENT_QUOTES,$encoding);}
		else
		{echo $rss_cat;}
		echo "</a></h1>\n";
	}

	// Titel, Link und Beschreibung der Items
	foreach ($items[1] as $item) {
		preg_match("/<title.*>(.+)<\/title>/Uism", $item, $title);
		if($atom == 0)
		{
			preg_match("/<link>(.+)<\/link>/Uism", $item, $link);
		}
		elseif($atom == 1)
		{
			preg_match("/<link.*alternate.*text\/html.*href=[\"\'](.+)[\"\'].*\/>/Uism", $item, $link);
		}

		if($atom == 0)
		{
			preg_match("/<description>(.*)<\/description>/Uism", $item, $description);
		}
		elseif($atom == 1)
		{
			preg_match("/<summary.*>(.*)<\/summary>/Uism", $item, $description);
		}

		$title = preg_replace('/<!\[CDATA\[(.+)\]\]>/Uism', '$1', $title);
		$description = preg_replace('/<!\[CDATA\[(.+)\]\]>/Uism', '$1', $description);
		$link = preg_replace('/<!\[CDATA\[(.+)\]\]>/Uism', '$1', $link);

		echo "<p class=\"link\">\n";
		echo "<a href=\"".$link[1]."\" title=\"";
		if($encode != "no")
		{echo htmlentities($title[1],ENT_QUOTES,$encoding);}
		else
		{echo $title[1];}
		echo "\">";
		if($encode != "no")
		{echo htmlentities($title[1],ENT_QUOTES,$encoding)."</a>\n";}
		else
		{echo $title[1]."</a>\n";}
		echo "</p>\n";
		if($mode == 2 || $mode == 3 && ($description[1]!="" && $description[1]!=" "))
		{
			echo "<p class=\"description\">\n";
			if($encode != "no")
			{echo htmlentities($description[1],ENT_QUOTES,$encoding)."\n";}
			else
			{echo $description[1];}
			echo "</p>\n";
		}
		if ($anzahl-- <= 1) break;
	}
	echo "</div>\n\n";
}



/**
 * List taxonomy
 */
function list_posts_by_taxonomy( $post_type, $taxonomy ){
  $terms = get_terms($taxonomy, array( 'parent' => 0 ) );
  foreach ($terms as $term) {
    echo "<li><a href='#'>" . $term->name . "</a>";
    $myposts = get_posts(array(
      'showposts' => -1,
      'post_type' => $post_type,
      'tax_query' => array(
          array(
          'taxonomy' => $taxonomy,
          'field' => 'slug',
          'terms' => $term->name)
      ))
    );
    echo "<ul>";

    //Get the Child terms
        $chterms = get_terms( $taxonomy, array( 'parent' => $term->term_id, 'orderby' => 'slug', 'hide_empty' => true ) );

        if (!empty($chterms)) {

        foreach ( $chterms as $chterm ) {
           echo "<li><a href='#'>" . $chterm->name . "</a>";  

            $mychposts = get_posts(array(
                'showposts' => -1,
                'post_type' => $post_type,
                'tax_query' => array(
                    array(
                    'taxonomy' => $taxonomy,
                    'field' => 'slug',
                    'terms' => $chterm->name)
                ))
            );
            echo "<ul>";
            foreach ($mychposts as $mychpost) {
            echo '<li><a href="'. get_permalink($mychpost->ID) .'">' . $mychpost->post_title . '</a></li>';
            }
            echo "</ul>";


        }
        
        } else {


        foreach ($myposts as $mypost) {
        echo '<li><a href="'. get_permalink($mypost->ID) .'">' . $mypost->post_title . '</a></li>';
        }
    }
    echo "</ul>";
  }
  echo '</li>';
}


/**
 * List taxonomy-archive for Hersteller (w/ categories)
 */
function list_posts_by_taxonomy_archive( $post_type, $taxonomy ){
  $terms = get_terms($taxonomy);
  foreach ($terms as $term) {
    echo "<li><a href='#'>" . $term->name . "</a>";
    $myposts = get_posts(array(
      'showposts' => -1,
      'post_type' => $post_type,
			'order'=> 'ASC',
			'orderby' => 'title',
      'tax_query' => array(
          array(
          'taxonomy' => $taxonomy,
          'field' => 'slug',
          'terms' => $term->name)
      ))
    );
    echo "<ul class='hersteller-list'>";
		$post_count = 0;
    foreach ($myposts as $mypost) {
			$post_count++;
			
  		    echo '<li><a href="'. get_bloginfo('url') .'/'. $taxonomy .'/'. $term->slug .'/#'. $post_count .'">' . $mypost->post_title . '</a></li>';
			
	}
    echo "</ul>";
  }
  echo '</li>';
}


/**
 * List posts of custom_post_type
 */
function list_posts_by_custom_post_type( $post_type ){
  $args=array(
    'post_type' => $post_type,
    'post_status' => 'publish',
    'posts_per_page' => -1,
    'caller_get_posts'=> 1,
    'orderby'=> 'date',
	'no_found_rows' => 'true',
	'posts_per_page' => '10'
  );

  $my_query = null;
  $my_query = new WP_Query($args);
  if( $my_query->have_posts() ) {
    while ($my_query->have_posts()) : $my_query->the_post(); ?>
      <li><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></li>
      <?php
    endwhile;
  }
  wp_reset_query();  // Restore global post data stomped by the_post().
}


/**
 * List posts of custom_post_type hersteller
 */
function list_posts_by_custom_post_type_hersteller( $post_type ){
  $args=array(
    'post_type' => $post_type,
    'post_status' => 'publish',
    'posts_per_page' => -1,
    'caller_get_posts'=> 1,
    'orderby'=> 'date',
	'no_found_rows' => 'true',
	'posts_per_page' => '15'
  );

  $my_query = null;
  $my_query = new WP_Query($args);
  if( $my_query->have_posts() ) {
    while ($my_query->have_posts()) : $my_query->the_post();
		$post_count++; ?>
      <li><a href="/hersteller/#<?php echo $post_count; ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></li>
	  <?php
    endwhile;
  }
  wp_reset_query();  // Restore global post data stomped by the_post().
}



			

/**
 * List posts of custom_post_type
 */
function get_first_of_custom_post_type( $post_type ){
  $args=array(
    'post_type' => $post_type,
    'post_status' => 'publish',
    'posts_per_page' => -1,
    'caller_get_posts'=> 1,
    'orderby'=> 'title',
    'order' => 'ASC'
  );

  $my_query = null;
  $my_query = new WP_Query($args);
  if( $my_query->have_posts() ) {
    while ($my_query->have_posts()) : $my_query->the_post();
		$postID = get_the_ID();
		$is_active = wp_get_post_categories( $postID );
		if ($is_active) {
		?>
      <a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>">Neu: <?php the_title(); ?></a>
      <?php
		}
    endwhile;
  }
  wp_reset_query();  // Restore global post data stomped by the_post().
}

/**
 * get current slug
 */
function get_slug(){
    global $post;
    $post_slug=$post->post_name;
    return $post_slug;
}

/**
 * Content Scheduler Bugfix
 */
add_action('admin_head', 'my_custom_fonts');

function my_custom_fonts() {
  echo '<style>
    #ContentScheduler_sectionid {
      z-index: 8888;
    }
  </style>';
}

/**
 * Customize Login
 */

function my_login_logo() { ?>
    <style type="text/css">

				@font-face{ /* for IE */
					font-family:MuseoSans;
					src:url(<?php echo get_stylesheet_directory_uri(); ?>/font/museosans100.eot);
					}
				@font-face { /* for non-IE */
					font-family:MuseoSans;
					src:url(http://:/) format("No-IE-404"),url(<?php echo get_stylesheet_directory_uri(); ?>/font/museosans100.ttf) format("truetype");
					}
				.login {
				  background: url(<?php echo get_stylesheet_directory_uri(); ?>/img/admin-login-bg.jpg) no-repeat center center fixed;
				  -webkit-background-size: cover;
				  -moz-background-size: cover;
				  -o-background-size: cover;
				  background-size: cover;
			    margin: 0;
			    height: 100%;
						font-family:MuseoSans;
				}
        .login h1 a {
            background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/img/logo_white.svg);
            padding-bottom: 30px;
				    width: 200px;
				    background-size: 200px;
				   pointer-events: none;
				   cursor: default;
        }
				.login form{
					background-color: rgba(255,255,255,0.9);
			    -webkit-box-shadow: none;
			    box-shadow: none;
    			padding: 26px 24px 16px;
				}
				.login form .forgetmenot {
			    display: none;
				}
				.login form #wp-submit {
			    width: 100%
				}
				.login #backtoblog {
					display: none;
				}
				.login #nav {
					text-align: center;
				}
				.login .message {
					border-left: 4px solid #70b940;
				}
				.wp-core-ui .button-primary {
				  background: #70b940;
				  border-color: #70b940;
				}
				.wp-core-ui .button-primary:hover {
				  background: #aaa;
				  border-color: #aaa;
				}
				.login #backtoblog a, .login #nav a, .login h1 a {
				    text-decoration: none;
				    color: #666;
				}
				.login #backtoblog a:hover, .login #nav a:hover, .login h1 a:hover {
				    text-decoration: none;
				    color: #70b940;
				}
				.login #nav {
					margin: 0;
					padding: 0 0 15px 0;
			    background-color: rgba(255,255,255,0.8);
				}
				#loginform input[type=text], #loginform input[type=password] {
				  -webkit-transition: all 0.30s ease-in-out;
				  -moz-transition: all 0.30s ease-in-out;
				  -ms-transition: all 0.30s ease-in-out;
				  -o-transition: all 0.30s ease-in-out;
				  outline: none;
    			padding: 5px 8px;
				  border: 1px solid #DDDDDD;
					font-size: 20px;
				}

				#loginform input[type=text]:focus, #loginform input[type=password]:focus {
				  box-shadow: none;
				  border: 1px solid rgba(112, 185, 64, 1);
				}
				.login label {
					color: #666;
				}
    </style>
<?php }
add_action( 'login_enqueue_scripts', 'my_login_logo' );


/**
 * get RSS-Feed Alternative
 */

function getFeedLink($feed_url) {

    $content = file_get_contents($feed_url);
    $x = new SimpleXmlElement($content);

    foreach($x->channel->item as $entry) {
        echo $entry->link;
				break;
    }
}

function getFeedLatestItem($feed_url) {

    $content = file_get_contents($feed_url);
    $x = new SimpleXmlElement($content);

    foreach($x->channel->item as $entry) {
        echo $entry->title;
    }
}

/*Contact form 7 remove span*/
add_filter('wpcf7_form_elements', function($content) {
    $content = preg_replace('/<(span).*?class="\s*(?:.*\s)?wpcf7-form-control-wrap(?:\s[^"]+)?\s*"[^\>]*>(.*)<\/\1>/i', '\2', $content);

    $content = str_replace('<br />', '', $content);
        
    return $content;
});

?>
