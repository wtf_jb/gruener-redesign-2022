<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Grüner
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer row" role="contentinfo">

		<div class="column socialmedia-icon">
			<a href="https://www.facebook.com/Grüner-GmbH-110870896154061/" target="_blank"><div class="facebook"></div></a>
			<a href="https://www.instagram.com/gruenergmbh/" target="_blank"><div class="instagram"></div></a>
			<a href="https://www.houzz.de/pro/gruenergmbh/gruener-gmbh?irs=US" target="_blank"><div class="houzz"></div></a>
			<a href="http://gruener-gerstetten.de/newsletter/"><div class="newsletter"></div></a>
		</div>

		<div class="column right">

			<?php wp_nav_menu( array('menu' => 'Footer' )); ?>

		</div>	

		<div class="footer_mobile">
			<?php wp_nav_menu( array('menu' => 'Footer' )); ?>
		</div>

	</footer><!-- #colophon -->
</div><!-- #page -->


<?php wp_footer(); ?>


</body>
</html>
