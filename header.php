 <?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Grüner
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

	<meta name="google-site-verification" content="eEdvSUN6giGSfw2PAKh1RoYEPXe_gGhh3TWQGmRDsj4" />


	<link rel="apple-touch-icon" sizes="57x57" href="<?php bloginfo( 'template_url' ); ?>/favicons/apple-touch-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="<?php bloginfo( 'template_url' ); ?>/favicons/apple-touch-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="<?php bloginfo( 'template_url' ); ?>/favicons/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="<?php bloginfo( 'template_url' ); ?>/favicons/apple-touch-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="<?php bloginfo( 'template_url' ); ?>/favicons/apple-touch-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="<?php bloginfo( 'template_url' ); ?>/favicons<?php bloginfo( 'template_url' ); ?>/favicons/apple-touch-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="<?php bloginfo( 'template_url' ); ?>/favicons/apple-touch-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="<?php bloginfo( 'template_url' ); ?>/favicons/apple-touch-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="<?php bloginfo( 'template_url' ); ?>/favicons/apple-touch-icon-180x180.png">
	<link rel="icon" type="image/png" href="<?php bloginfo( 'template_url' ); ?>/favicons/favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="<?php bloginfo( 'template_url' ); ?>/favicons/android-chrome-192x192.png" sizes="192x192">
	<link rel="icon" type="image/png" href="<?php bloginfo( 'template_url' ); ?>/favicons/favicon-96x96.png" sizes="96x96">
	<link rel="icon" type="image/png" href="<?php bloginfo( 'template_url' ); ?>/favicons/favicon-16x16.png" sizes="16x16">
	<link rel="manifest" href="<?php bloginfo( 'template_url' ); ?>/favicons/manifest.json">
	<link rel="mask-icon" href="<?php bloginfo( 'template_url' ); ?>/favicons/safari-pinned-tab.svg" color="#5bbad5">
	<meta name="msapplication-TileColor" content="#000000">
	<meta name="msapplication-TileImage" content="<?php bloginfo( 'template_url' ); ?>/favicons/mstile-144x144.png">
	<meta name="theme-color" content="#000000">


	<link rel="stylesheet" href="<?php bloginfo( 'template_url' ); ?>/css/superslides.css" type="text/css" media="screen" />
	<?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>
<div id="wptime-plugin-preloader"></div>
<div id="page" class="hfeed site">

	<header class="row">	
		<div class="column">
			<span class="main-menu-toggle"><img src="<?php bloginfo( 'template_url' ); ?>/img/hamburger.png" class="hamburger" alt="Icon Menü Grüner GmbH Gerstetten">men&uuml;</span>
			<a class="reference-back-url"><span class="reference-back"><img src="<?php bloginfo( 'template_url' ); ?>/img/grid.png" class="reference-back-x">&Uuml;bersicht</span></a>
		</div>
		<div class="column right"><a href="/"><img src="<?php bloginfo( 'template_url' ); ?>/img/logo_black.png" class="logo" alt="Logo der Grüner GmbH Gerstetten"></a></div>
	</header>

	<?php $postid = get_the_ID(); ?>

	<div class="navigation">
		<div class="x"><img src="<?php bloginfo( 'template_url' ); ?>/img/x-black.png" class="x"></div>
		<div class="nav-container">        
			<ul class="main-nav-element">
				<li><a tabindex="-1" href="#">Leistungen</a>
					<ul>
						<li><a tabindex="-1" href="#">K&uuml;chen</a>
							<ul>
								<li><a href="<?php $i='1009'; echo esc_url(get_permalink($i)); ?>" class="<?php if ($postid == $i){echo 'active';} ?>">Schreinerk&uuml;chen</a></li>
								<li><a href="<?php $i='1013'; echo esc_url(get_permalink($i)); ?>" class="<?php if ($postid == $i){echo 'active';} ?>">Bulthaup K&uuml;chen</a></li>
								<li><a href="<?php $i='1017'; echo esc_url(get_permalink($i)); ?>" class="<?php if ($postid == $i){echo 'active';} ?>">Leicht K&uuml;chen</a></li>
							</ul>
						</li>
						<li><a href="<?php $i='250'; echo esc_url(get_permalink($i)); ?>" class="<?php if ($postid == $i){echo 'active';} ?>">Innenarchitektur</a></li>
						<li><a href="<?php $i='255'; echo esc_url(get_permalink($i)); ?>" class="<?php if ($postid == $i){echo 'active';} ?>">Schreinerei</a></li>
						<li><a href="#">Unsere Ausstellung</a>
							<ul>
								<li><a href="<?php $i='1373'; echo esc_url(get_permalink($i)); ?>" class="<?php if ($postid == $i){echo 'active';} ?>">k&uuml;chenausstellung</a></li>
								<li><a href="<?php $i='247'; echo esc_url(get_permalink($i)); ?>" class="<?php if ($postid == $i){echo 'active';} ?>">M&ouml;belausstellung</a>
							</ul>
						</li>
						<li class="hersteller"><a href="#">Hersteller (Auswahl)</a>
							<ul>
								<?php list_posts_by_custom_post_type_hersteller( 'hersteller'); ?>
							</ul>
							
						</li>
						<li><a href="<?php $i='299'; echo esc_url(get_permalink($i)); ?>" class="<?php if ($postid == $i){echo 'active';} ?>">bulthaup bei grüner</a></li>
						<li><a href="<?php $i='257'; echo esc_url(get_permalink($i)); ?>" class="<?php if ($postid == $i){echo 'active';} ?>">design m&ouml;bel outlet</a></li>
					</ul>
				</li>
				<li><a href="/ref" class="<?php if ($postid == '1914'){echo 'active';} ?>" >Referenzen</a></li>
				<li><a href="#">&Uuml;ber uns</a>
					<ul>
						<li><a href="#">Team</a>
							<ul>
								<li><a href="<?php $i='847'; echo esc_url(get_permalink($i)); ?>" class="<?php if ($postid == $i){echo 'active';} ?>">Planung und Beratung</a></li>
								<li><a href="<?php $i='850'; echo esc_url(get_permalink($i)); ?>" class="<?php if ($postid == $i){echo 'active';} ?>">Werkstatt</a></li>
								<li><a href="<?php $i='853'; echo esc_url(get_permalink($i)); ?>" class="<?php if ($postid == $i){echo 'active';} ?>">Backoffice</a></li>
							</ul>
						</li>
						<li><a href="<?php $i='992'; echo esc_url(get_permalink($i)); ?>" class="<?php if ($postid == $i){echo 'active';} ?>">Newsletter</a></li>
					</ul>
				</li>
				<?php $job_page = get_page('253'); if ($job_page->post_status == 'publish') { ?>
				<li class="jobs"><a href="#">Jobs</a>
					<ul>
						<?php list_posts_by_custom_post_type( 'jobs'); ?>
					</ul>
				</li>
				<?php } ?>
				<li><a href="<?php $i='13'; echo esc_url(get_permalink($i)); ?>" class="<?php if ($postid == $i){echo 'active';} ?>">Kontakt</a></li>
				<li class="nav_terminvereinbarung"><a href="<?php $i='1854'; echo esc_url(get_permalink($i)); ?>" class="<?php if ($postid == $i){echo 'active';} ?>">Terminvereinbarung</a></li>
				<li class="neuigkeit"><?php get_first_of_custom_post_type('neuigkeiten'); ?></li>
			</ul>
		</div>
	</div>
<div id="content" class="site-content">
