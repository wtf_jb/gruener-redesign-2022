/**
 * gruener-scripts.js
 *
 * Handles all custom scripts
 */


/*
* Hide Logo in mobile fullscreen
*/

// device detection
var isMobile = false; //initiate as false
if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4))) isMobile = true;

$(document).ready(function() {
   if(isMobile){
       if ($("body").hasClass("design-moebel-outlet")) {
           $('.logo').click(function () {
           $('.entry-content').toggleClass( "overflow-hidden" );
           });
       }
       if ($("body").hasClass("ref")) {
           $('.wpupg-grid-filters').addClass( "mobile" );
       }
   }
});


/*
* Remove preserve class on mobile
*/

$(document).ready(function() {
    if(isMobile){
        $(".team img").removeClass('preserve');
    }
 });




/*
* Superslides
*/

$(document).on('animated.slides', function(slide) {
    slideIndex = $('#slides').superslides('current');
    slideIndex = $('#event-slides').superslides('current');
    $(".current_image").empty();
    $(".current_image").append(slideIndex+1);
 });
 
 numberOfSlides = $('#slideshow .slides-container > li, #banner .slides-container > li').length;
 if(numberOfSlides > 1){
    playThis = 8000;
 } else {
    playThis = 0;
 }
 
 $(document).ready(function() {
    jQuery('#slides').superslides({
        hashchange: true
    });
 });
 
 $(document).ready(function() {
    jQuery('#event-slides').superslides({
        hashchange: true,
        //play: 4000
    });
 });




 $(document).ready(function() {
    jQuery( ".column.socialmedia-icon" ).click(function() {
        jQuery('#slides').superslides('animate', 'prev');
    });
    jQuery( ".column.right" ).click(function() {
        jQuery('#slides').superslides('animate', 'next');
    });
    jQuery("#slides").swipe( {
            swipeLeft:function(event, direction, distance, duration, fingerCount, fingerData) {
                jQuery(this).superslides('animate', 'next');
            },
            swipeRight:function(event, direction, distance, duration, fingerCount, fingerData) {
                jQuery(this).superslides('animate', 'prev');
            },
            threshold:5
        });

 });




/*
* Rapidmail Newsletter
*/

$(document).ready(function() {
   $("#rm-firstname").attr("placeholder", "Vorname");
   $("#rm-lastname").attr("placeholder", "Nachname");
   $("#rm-email").attr("placeholder", "E-Mail");
}); 

 
/*
 * Get Referenzen URL, store it in Cookie and use it for Referenzen-Übersicht
 */


$("#wpupg-grid-111 .wpupg-item").click(function(){
    var url = window.location.href; // Returns full URL (https://example.com/path/example.html)
    Cookies.set('uebersicht-url', url);
});

$(".reference-back-url").click(function(){
    window.location.href = Cookies.get('uebersicht-url');
});





/*
 * Tooltip
 */




 $(document).ready(function() {
    if(!isMobile){
        $(".tooltip-clickable").click(function(){
            $(".tooltip-content-fullscreen, .tooltip-content").fadeToggle("fast", function(){});
            if($(".tooltip-content-fullscreen, .tooltip-content").hasClass('visible')){
                $(".tooltip-content-fullscreen, .tooltip-content").removeClass('visible');
                $(".tooltip").removeClass('active');
            } else {
                $(".tooltip-content-fullscreen, .tooltip-content").addClass('visible');
                $(".tooltip").addClass('active');
            }
        });
    }
});

 $(document).ready(function() {
    if(isMobile){
        $(".tooltip").removeClass('active');
        $(".tooltip-content-fullscreen-mobile, .tooltip-content, .tooltip-content.hersteller").removeClass('visible');
        $(".tooltip-content.team").addClass('visible');
        $(".tooltip-clickable").click(function(){
            $(".tooltip-content-fullscreen-mobile, .tooltip-content.hersteller").fadeToggle("fast", function(){});
            if($(".tooltip-content-fullscreen-mobile, .tooltip-content.hersteller").hasClass('visible')){
                $(".tooltip-content-fullscreen-mobile, .tooltip-content.hersteller").removeClass('visible');
                $(".tooltip").removeClass('active');
            } else {
                $(".tooltip-content-fullscreen-mobile, .tooltip-content.hersteller").addClass('visible');
                $(".tooltip").addClass('active');
            }
        });
    }
 });





// Event "Lieblingsstücke"
$("body.2015-lieblingsstuecke").keydown(function(e) {
    if(e.keyCode == 37) { // left
    $(".tooltip-content-fullscreen").css("display", "none");
    $(".tooltip-content").css("display", "none");
        $('.tooltip').css("background-image", "url(<?php bloginfo( 'template_url' ); ?>/img/gruener_icon_plus.svg)");
    }
    if(e.keyCode == 39) { // right
    $(".tooltip-content-fullscreen").css("display", "none");
    $(".tooltip-content").css("display", "none");
        $('.tooltip').css("background-image", "url(<?php bloginfo( 'template_url' ); ?>/img/gruener_icon_plus.svg)");
    }
});
$("body.2015-lieblingsstuecke .next").click(function(){
    $(".tooltip-content-fullscreen").css("display", "none");
    $(".tooltip-content").css("display", "none");
    $('.tooltip').css("background-image", "url(<?php bloginfo( 'template_url' ); ?>/img/gruener_icon_plus.svg)");
});
$("body.2015-lieblingsstuecke .prev").click(function(){
    $(".tooltip-content-fullscreen").css("display", "none");
    $(".tooltip-content").css("display", "none");
    $('.tooltip').css("background-image", "url(<?php bloginfo( 'template_url' ); ?>/img/gruener_icon_plus.svg)");
});


/*
 * Navigation
 */

// Navigation overlay toggle
$( ".main-menu-toggle, .navigation, .nav-container li.hersteller ul li a" ).click(function() {
    $( ".navigation" ).fadeToggle( "fast", "linear" );
    $( ".main-menu-toggle" ).fadeToggle( "fast", "linear" );
});

// remove "active" class
$( ".nav-container li.hersteller ul li a" ).click(function() {
    $( ".main-menu-toggle, .navigation, .nav-container li.hersteller ul li" ).removeClass( 'active' );
    $( ".main-menu-toggle, .navigation, .nav-container li.hersteller ul li a" ).removeClass( 'active' );
});

// Active navigation elements
$(document).ready(function() {
    active_menu_cb = function(e, submenu) {
        $('#main-nav-element').find('li').removeClass('active');
        var l = $(this);
        var li =  $(this).parent();
        var lis = li.parents('li');
        l.addClass('active');
        li.addClass('active');
        lis.addClass('active');
    };
    jQuery(".main-nav-element").navgoco({accordion: true, slide: {duration: 100, easing: 'linear'}, onClickAfter: active_menu_cb});
}); 


/*
 * Referenzen-Grid
 */

// Overlay
$(".wpupg-item").hover(function(){
    $(this).children('.referenzen_overlay').toggleClass("display-block");
});

// Filter
$(document).ready(function(){ 
    $( ".wpupg-filter-checkboxes-term-level-0" ).parent().addClass("level-0"); 
    $( ".wpupg-filter-checkboxes-term-level-1" ).parent().addClass("level-1"); 
});
 
// mobile Filter
$(document).ready(function(){ 
    $( ".ref_mobile_filter, .ref_mobile_applyFilter" ).click(function() {
        $( ".wpupg-grid-filters" ).fadeToggle( "fast", "linear" );
        $( ".ref_mobile_overlay" ).fadeToggle( "fast", "linear" );
        $('body').toggleClass( "overflow-hidden" );
    });
});




/*
 * Obsolete stuff
 */
 


/*
 * Ochio Campaign
 */
/*
$(document).ready(function() {
    $('body.occhio .content-area .slides-container').click(function(){
        window.open('https://de.occhio.de/online-shop?pid=1711a4&utm_source=partner&utm_campaign=Kampagne&utm_medium=1711a4-Grüner+GmbH&utm_term=1711a4&utm_content=myOcchio_Occhio_Partner_Campaign_full2880.jpg');
    });
    $("body.occhio .content-area .slides-container").css('cursor', 'pointer');
}); 
*/

/*
//Event Popup always visible 
$(document).ready(function() {
    if ($("body").hasClass("single-events")) {
        $(".tooltip-content-fullscreen").fadeToggle("fast", function(){});
        $('.tooltip').css("background-image", "url(<?php bloginfo( 'template_url' ); ?>/img/gruener_icon_kreuz.svg)");
        $(".tooltip-content-fullscreen").addClass('visible');
        if(isMobile){
            $('.logo').css('display', 'none')
        }
    }
});
*/
/*
$(".tooltip-content-fullscreen").click(function(){
    $(".tooltip-content-fullscreen").fadeToggle("fast", function(){});
    if($(".tooltip-content-fullscreen").is(':visible')){
        if($(".tooltip-content-fullscreen").hasClass('visible')){
            $('.tooltip').css("background-image", "url(<?php bloginfo( 'template_url' ); ?>/img/gruener_icon_plus.svg)");
            if(isMobile){
                $('.logo').css('display', 'block')
            }
            $(".tooltip-content-fullscreen").removeClass('visible');
        } else {
            $('.tooltip').css("background-image", "url(<?php bloginfo( 'template_url' ); ?>/img/gruener_icon_kreuz.svg)");
            if(isMobile){
                $('.logo').css('display', 'none')
            }
            $(".tooltip-content-fullscreen").addClass('visible');
        }
    }
});
*/
/*     
$(".impressum .tooltip-content-fullscreen").click(function(){
    $(".tooltip-content-fullscreen").fadeToggle("fast", function(){});
    if($(".tooltip-content-fullscreen").is(':visible')){
        if($(".tooltip-content-fullscreen").hasClass('visible')){
            $('.tooltip').css("background-image", "url(<?php bloginfo( 'template_url' ); ?>/img/gruener_icon_plus.svg)");
            $(".tooltip-content-fullscreen").removeClass('visible');
            if(isMobile){
                $('.logo').css('display', 'block')
            }
        }
    } else {
        $('.tooltip').css("background-image", "url(<?php bloginfo( 'template_url' ); ?>/img/gruener_icon_kreuz.svg)");
        $(".tooltip-content-fullscreen").addClass('visible');
        if(isMobile){
            $('.logo').css('display', 'none')
        }
    }
});
*/   
/*     
$(".kontakt .tooltip-content-fullscreen").click(function(){
    $(".tooltip-content-fullscreen").fadeToggle("fast", function(){});
    if($(".tooltip-content-fullscreen").is(':visible')){
        if($(".tooltip-content-fullscreen").hasClass('visible')){
            $('.tooltip').css("background-image", "url(<?php bloginfo( 'template_url' ); ?>/img/gruener_icon_plus.svg)");
            $(".tooltip-content-fullscreen").removeClass('visible');
            if(isMobile){
                $('.logo').css('display', 'block')
            }
        }
    } else {
            $('.tooltip').css("background-image", "url(<?php bloginfo( 'template_url' ); ?>/img/gruener_icon_kreuz.svg)");
            $(".tooltip-content-fullscreen").addClass('visible');
            if(isMobile){
                $('.logo').css('display', 'none')
            }
    }
});
*/
 
/*
$(document).ready(function() {
    if(localStorage.getItem('navigation_hint') != 'shown'){ //only show hint if it wasn't shown before
        $(".navigation_hint").delay(10000).fadeIn("slow"); //show hint after 10 seconds
        localStorage.setItem('navigation_hint','shown') //localStorage that hint was shown
    }

    $(document).click(function() {
        $(".navigation_hint").fadeOut("slow"); //fade out hint if click anywhere in page
    });
});
*/

/*
$(".impressum .tooltip-clickable, .kontakt .tooltip-clickable").click(function(){
    if($(".tooltip-content-fullscreen").is(':visible')){
        if($(".tooltip-content-fullscreen").hasClass('visible')){
            $('.tooltip').css("background-image", "url(<?php bloginfo( 'template_url' ); ?>/img/gruener_icon_plus.svg)");
            if(isMobile){
                $('.logo').css('display', 'block')
            }
        } else {
            $('.tooltip').css("background-image", "url(<?php bloginfo( 'template_url' ); ?>/img/gruener_icon_kreuz.svg)");
            if(isMobile){
                $('.logo').css('display', 'none')
            }
        }
    } else {
        $('.tooltip').css("background-image", "url(<?php bloginfo( 'template_url' ); ?>/img/gruener_icon_kreuz.svg)");
        if(isMobile){
            $('.logo').css('display', 'none')
        }
    }
});
*/

/*
$(".kontakt .tooltip-clickable").click(function(){
    if($(".tooltip-content-fullscreen").is(':visible')){
        if($(".tooltip-content-fullscreen").hasClass('visible')){
            $('.tooltip').css("background-image", "url(<?php bloginfo( 'template_url' ); ?>/img/gruener_icon_plus.svg)");
            if(isMobile){
                $('.logo').css('display', 'block')
            }
        } else {
            $('.tooltip').css("background-image", "url(<?php bloginfo( 'template_url' ); ?>/img/gruener_icon_kreuz.svg)");
            if(isMobile){
                $('.logo').css('display', 'none')
            }
        }
    } else {
        $('.tooltip').css("background-image", "url(<?php bloginfo( 'template_url' ); ?>/img/gruener_icon_kreuz.svg)");
        if(isMobile){
            $('.logo').css('display', 'none')
        }
    }
});
*/