<?php
/**
 * Template part for displaying page content in page.php.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Grüner
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

<?php if(is_page( 'design-moebel-outlet' )){ ?>

    <div class="entry-content">

      <?php
          $x = new SimpleXmlElement(file_get_contents("http://design-moebeloutlet.de/?cat=". get_field('feld_1') ."&feed=rss2"));
          foreach($x->channel->item as $entry) {
            $entry_link = $entry->link;
            $entry_title = $entry->title;
            $entry_cat = preg_replace('/<!\[CDATA\[(.+)\]\]>/Uism', '$1', $entry->category);
            break;
          }
      ?>
        <a href="<?php echo $entry_link; ?>" class="dmo-box-link" target="_blank">
          <div class="dmo-box dmo<?php the_field('feld_1'); ?>">
              <div class="overlay"></div>
              <div class="content">
                <h1><?php echo $entry_cat; ?></h1>
                <p><?php echo $entry_title; ?></p>
              </div>
          </div>
        </a>
      <?php
      ?>

      <?php
          $x = new SimpleXmlElement(file_get_contents("http://design-moebeloutlet.de/?cat=". get_field('feld_2') ."&feed=rss2"));
          foreach($x->channel->item as $entry) {
            $entry_link = $entry->link;
            $entry_title = $entry->title;
            $entry_cat = preg_replace('/<!\[CDATA\[(.+)\]\]>/Uism', '$1', $entry->category);
            break;
          }
      ?>
        <a href="<?php echo $entry_link; ?>" class="dmo-box-link" target="_blank">
          <div class="dmo-box dmo<?php the_field('feld_2'); ?>">
              <div class="overlay"></div>
              <div class="content">
                <h1><?php echo $entry_cat; ?></h1>
                <p><?php echo $entry_title; ?></p>
              </div>
          </div>
        </a>
      <?php
      ?>

      <div class="dmo-box dmo-logo"></div>

      <?php
          $x = new SimpleXmlElement(file_get_contents("http://design-moebeloutlet.de/?cat=". get_field('feld_3') ."&feed=rss2"));
          foreach($x->channel->item as $entry) {
            $entry_link = $entry->link;
            $entry_title = $entry->title;
            $entry_cat = preg_replace('/<!\[CDATA\[(.+)\]\]>/Uism', '$1', $entry->category);
            break;
          }
      ?>
        <a href="<?php echo $entry_link; ?>" class="dmo-box-link" target="_blank">
          <div class="dmo-box dmo<?php the_field('feld_3'); ?>">
              <div class="overlay"></div>
              <div class="content">
                <h1><?php echo $entry_cat; ?></h1>
                <p><?php echo $entry_title; ?></p>
              </div>
          </div>
        </a>
      <?php
      ?>
    </div>

    <?php } elseif(is_page( 'newsletter' )){ ?>

	<div class="entry-content">

		<?php the_field('newsletter_shortcode'); ?>

	</div>
	
	<?php } elseif(is_page( 'termin' ) || is_page( 'terminvereinbarung' ) || is_page( 'terminvereinbarung-2' ) || is_page( 'terminvereinbarung-3' ) || is_page( 'terminvereinbarung-4' ) || is_page( 'terminvereinbarung-5' ) || is_page( 'gewinnspiel' )){ ?>

	<div class="entry-content">

		<?php the_field('form_shortcode'); ?>

	</div>

  			<?php } elseif(is_page( 'startseite' )) { ?>
	
	<!-- Modal HTML embedded directly into document -->
<div id="ex1" class="modal">
  <p>Thanks for clicking. That felt good.</p>
  <a href="#" rel="modal:close">Close</a>
</div>

				<?php if (count(get_field('bg-slider')) > 1) { $slider_class = "has_slider";} ?>
						<div id="slides" class="bg-slider referenzen <?php echo $slider_class; ?>">
								<ul class="slides-container">

									<?php

									if(get_field('latest_event')) {

										$event_filter = get_field('event_filter');

										$post = $event_filter;

										setup_postdata( $post );

											$images = get_field('bg-slider');

											$image_id = $images[0];

											if( $image_id ):
											?>
												<li>
													<a href="<?php the_permalink(); ?>/#2"><img src="<?php echo $image_id['url']; ?>" alt="<?php echo $image_id['alt']; ?>"></a>

													<div class="tooltip-wrapper">
														<div class="tooltip">
															<div class="tooltip-clickable"></div>
														</div>
													</div>
													<div class="tooltip-content-fullscreen">
														<div class="desc">
															<div class="desc_relative">
																<span class="title"><?php the_field('headline'); ?></span></ br>
														<?php the_field('description'); ?>
													</div>
														</div>
													</div>

												</li>

											<?php

										wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly
										endif;

									} else {


										$rows = get_field('referenzen_filter');
										if($rows)
										{
											shuffle( $rows );

											$row = $rows[0];
											$hersteller_nr = $row['referenz_nr'];
											$post = $hersteller_nr;
											setup_postdata( $post );

												$bg_images = get_field('bg-slider');
												$bg_image = $bg_images[0];

												?>
													<li>
														<a href="<?php the_permalink(); ?>"><img src="<?php echo $bg_image['url']; ?>" ></a>
													</li>

												<?php

											wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly
										}
									}
									?>

								</ul>

								<nav class="slides-navigation">
									<a href="#" class="next">Next</a>
									<a href="#" class="prev">Previous</a>
								</nav>
							</div>

			<?php } elseif(is_page( 'impressum' )) { ?>

				<?php if (count(get_field('bg-slider')) > 1) { $slider_class = "has_slider";} ?>
						<div id="slides" class="bg-slider referenzen <?php echo $slider_class; ?>">
								<ul class="slides-container impressum">
									<?php
									// name of gallery-field
									$images = get_field('bg-slider');
									// check if image is not empty
									if( $images ):
										// loop through the images
										foreach( $images as $image ):
									?>
												<li>
													<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>">
													<div class="tooltip-wrapper">
														<div class="tooltip">
															<div class="tooltip-clickable"></div>
														</div>
													</div>
													<div class="tooltip-content-fullscreen">
														<div class="desc">
															<div class="desc_relative">
																<span class="title"><?php the_field('headline'); ?></span></ br>
														<?php the_field('description'); ?>
													</div>
														</div>
													</div>

												</li>


												<?php
												endforeach;
												endif;
												?>
											</ul>

											<nav class="slides-navigation">
												<a href="#" class="next">Next</a>
												<a href="#" class="prev">Previous</a>
											</nav>
										</div>
            <?php } elseif(is_page( 'datenschutz' )) { ?>

				<?php if (count(get_field('bg-slider')) > 1) { $slider_class = "has_slider";} ?>
						<div id="slides" class="bg-slider referenzen <?php echo $slider_class; ?>">
								<ul class="slides-container datenschutz">
									<?php
									// name of gallery-field
									$images = get_field('bg-slider');
									// check if image is not empty
									if( $images ):
										// loop through the images
										foreach( $images as $image ):
									?>
												<li>
													<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>">
													<div class="tooltip-wrapper">
														<div class="tooltip">
															<div class="tooltip-clickable"></div>
														</div>
													</div>
													<div class="tooltip-content-fullscreen">
														<div class="desc">
															<div class="desc_relative">
																<span class="title"><?php the_field('headline'); ?></span></ br>
														<?php the_field('description'); ?>
													</div>
														</div>
													</div>

												</li>


												<?php
												endforeach;
												endif;
												?>
											</ul>

											<nav class="slides-navigation">
												<a href="#" class="next">Next</a>
												<a href="#" class="prev">Previous</a>
											</nav>
										</div>


			<?php } elseif(is_page('kontakt')) {
				$kontaktbild = get_field('kontaktbild');
				?>
				<?php if (count(get_field('bg-slider')) > 1) { $slider_class = "has_slider";} ?>
						<div id="slides" class="bg-slider referenzen <?php echo $slider_class; ?>">
								<ul class="slides-container kontakt">
									<?php
									// name of gallery-field
									$images = get_field('bg-slider');
									// check if image is not empty
									if( $images ):
										// loop through the images
										foreach( $images as $image ):
									?>
												<li>
													<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>">
													<div class="tooltip-wrapper">
														<div class="tooltip">
															<div class="tooltip-clickable"></div>
														</div>
													</div>
													<div class="tooltip-content-fullscreen">
														<div class="desc">
															<div class="desc_relative">
																<div class="title"><?php the_field('headline'); ?></div></ br>
																<div class="kontaktdaten"><?php the_field('description'); ?></div></ br>
																<div class="ansprechpartner">
																	<img src="<?php echo $kontaktbild['url']; ?>" alt="<?php echo $kontaktbild['alt']; ?>" class="kontaktbild"></ br>
																	<div class="ansprechpartner_content"><?php the_field('description2'); ?></div></ br>
																</div>
															<div class="oeffnungszeiten"><?php the_field('description3'); ?></div></ br>
													</div>
														</div>
													</div>

												</li>


												<?php
												endforeach;
												endif;
												?>
											</ul>

											<nav class="slides-navigation">
												<a href="#" class="next">Next</a>
												<a href="#" class="prev">Previous</a>
											</nav>
										</div>


			<?php } elseif(is_page('lichtplanung') || is_page('kueche') || is_page('schreinerkueche') || is_page('bulthaup-kuechen') || is_page('leicht-kuechen')) { ?>

<?php if (count(get_field('bg-slider')) > 1) { $slider_class = "has_slider";} ?>
		<div id="slides" class="bg-slider referenzen <?php echo $slider_class; ?>">
			<ul class="slides-container">
				<?php $images = get_field('bg-slider');
				if( $images ): foreach( $images as $image ): ?>

					<li><img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>">

					<div class="tooltip-content-fullscreen">
						<div class="desc">
							<span class="title"><?php the_field('headline'); ?></span></ br>
							<?php the_field('description'); ?>
							<?php if ($image['caption']) { ?>
							<p><a href="<?php bloginfo("url") ?>/<?php echo $image['caption']; ?>">> zum Referenzobjekt</a></p>
							<?php } ?>
						</div>
					</div>


					</li>



				<?php endforeach; endif; ?>
			</ul>

			<nav class="slides-navigation"><a href="#" class="next">Next</a><a href="#" class="prev">Previous</a></nav>
		</div>

		<?php $pagination_images = get_field('bg-slider');
		$ct_pg_img = count($pagination_images);
		if ($ct_pg_img > 1) {?>
		<div class="new-slides-pagination"><span class="current_image"></span><span class="seperator">&#124;</span><?php echo $ct_pg_img; ?></div>
		<?php } ?>

			<div class="tooltip-wrapper">
				<div class="tooltip">
					<div class="tooltip-clickable"></div>
				</div>
			</div>





      <?php } elseif(is_page( 'backoffice' ) || is_page( 'planung-und-beratung' ) || is_page( 'werkstatt' ) || is_page( 'bulthaup-bei-gruener' ) ) { ?>
        <?php if (count(get_field('bg-slider')) > 1) { $slider_class = "has_slider";} ?>
        <div id="slides" class="bg-slider events special-event ?> <?php echo $slider_class; ?>">
          <ul class="slides-container">

            <?php
            $image_count = 0;
            $images = get_field('bg-slider');
            if( $images ):
              foreach( $images as $image ):
                $image_count++;?>

                <li class="<?php echo "slide_nr_" . $image_count; ?>">
                  <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>">

                <?php
                  if ($image_count == 1) { ?>
                    <div class="tooltip-content-fullscreen">
                      <div class="desc"><span class="title"><?php the_field('headline'); ?></span></ br>
                      <?php the_field('description'); ?>
                      </div>
                    </div>
                  <?php } ?>
                    <div class="tooltip-wrapper">
                      <div class="tooltip">
                        <?php if ($image_count > 1) { ?>
                          <div class="tooltip-content">
                            <div class="title"><?php echo $image['title']; ?></div></ br>
                              <div class="desc"><?php echo $image['description']; ?></div></ br></ br>
                              <div class="desc"><?php echo $image['caption']; ?></div>
                          </div>
                        <?php } ?>
                        <div class="tooltip-clickable"></div>
                      </div>
                    </div>
              </li>
              <?php
              endforeach;
            endif; ?>
          </ul>

          <nav class="slides-navigation">
            <a href="#" class="next">Next</a>
            <a href="#" class="prev">Previous</a>
          </nav>
        </div>

        <?php $pagination_images = get_field('bg-slider');
        $ct_pg_img = count($pagination_images);
        if ($ct_pg_img > 1) {?>
        <div class="new-slides-pagination">
          <span class="current_image"></span>
          <span class="seperator">&#124;</span>
          <?php echo $ct_pg_img; ?>
        </div>
        <?php } ?>

	<?php } else {?>

<?php if (count(get_field('bg-slider')) > 1) { $slider_class = "has_slider";} ?>
		<div id="slides" class="bg-slider referenzen <?php echo $slider_class; ?>">
			<ul class="slides-container">
				<?php $images = get_field('bg-slider');
				if( $images ): foreach( $images as $image ): ?>

					<li><img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>"></li>



					<?php if (get_field('fullscreen_content') && is_page('lichtplanung') && is_page('kueche') && is_page('schreinerkueche') && is_page('bulthaup-kuechen') && is_page('leicht-kuechen')) : ?>
					<div class="tooltip-content-fullscreen">
						<div class="desc"><span class="title"><?php the_field('headline'); ?></span></ br>
						<?php the_field('description'); ?></div>
						</div>
					</div>
					<?php endif; ?>

				<?php endforeach; endif; ?>
			</ul>

			<nav class="slides-navigation"><a href="#" class="next">Next</a><a href="#" class="prev">Previous</a></nav>
		</div>

		<?php $pagination_images = get_field('bg-slider');
		$ct_pg_img = count($pagination_images);
		if ($ct_pg_img > 1) {?>
		<div class="new-slides-pagination"><span class="current_image"></span><span class="seperator">&#124;</span><?php echo $ct_pg_img; ?></div>
		<?php } ?>

			<div class="tooltip-wrapper">
				<div class="tooltip">
					<?php if (!get_field('fullscreen_content') ) : ?>
						<div class="tooltip-content">
							<div class="title"><?php if(get_field('headline')){the_field('headline');}else{the_title();}; ?></div></ br>
							<div class="desc"><?php the_field('description'); ?></div>
						</div>
					<?php endif; ?>
					<div class="tooltip-clickable"></div>
				</div>
			</div>


	<?php } ?>

</article><!-- #post-## -->



<?php if (get_field('fullscreen_content') && !is_page('lichtplanung') && !is_page('kueche') && !is_page('schreinerkueche') && !is_page('bulthaup-kuechen') && !is_page('leicht-kuechen') && !is_page('backoffice') && !is_page('werkstatt') && !is_page('planung-und-beratung')  && !is_page('bulthaup-bei-gruener'))   : ?>
<div class="tooltip-content-fullscreen">
	<div class="desc"><span class="title"><?php the_field('headline'); ?></span></ br>
	<?php the_field('description'); ?></div>
	</div>
</div>
<?php endif; ?>
